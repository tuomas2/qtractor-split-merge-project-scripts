#!/usr/bin/python 
# encoding: utf-8
# (C) 2013, Tuomas Airaksinen
# Licenced under GPL
# 
# Merge several QTractor projects (xml files) into one.
# Requires python-lxml package.
# 
# Assumes that last clip of each subproject ends in bar division and calculates the length of 
# the project from that.
#
# Assumes that track and bus names are unique.
#

from lxml import etree,objectify
from copy import deepcopy
import sys

if len(sys.argv) < 2: 
    print sys.argv[0],"out.qtr project1.qtr project2.qtr ..."
    sys.exit(0)

qtrs = sys.argv[2:]
    
print "reading base", qtrs[0]
baseroot = objectify.parse(qtrs[0]).getroot()
# there, in this baseroot, we will add entries from all the other files.

def projectlength(root):
    _projectlength = 0
    for c in root.findall("tracks/track/clips/clip"):
        end = c.properties.start+c.properties.length
        if end > _projectlength:
            _projectlength = end
    return _projectlength

project_end_now = projectlength(baseroot)

def temponode_from_project(root):
    temponode_xml = (
         """<tempo-node frame="170667" bar="-1">
           <tempo>135</tempo>
           <beat-type>2</beat-type>
           <beats-per-bar>7</beats-per-bar>
           <beat-divisor>2</beat-divisor>
          </tempo-node>""")

    temponode = objectify.fromstring(temponode_xml)
    temponode.attrib["frame"] = str(project_end_now)
    temponode.tempo = int(root.properties.tempo)
    temponode["beats-per-bar"] = int(root.properties["beats-per-bar"])
    temponode["beat-divisor"] = int(root.properties["beat-divisor"])
    return temponode
def process_qtr(fname):
    global project_end_now
    root = objectify.parse(fname).getroot()
    subprojectlength = projectlength(root)
    
    #first let's ensure that we will have all the files in filelists
    for f in root.files["audio-list"]["file"]:
        if baseroot.files["audio-list"].find("file[@name='%s']"%f.attrib["name"]) == None:
            baseroot.files["audio-list"].append(deepcopy(f)) 
    for f in root.files["midi-list"]["file"]:
        if baseroot.files["midi-list"].find("file[@name='%s']"%f.attrib["name"]) == None:
            baseroot.files["midi-list"].append(deepcopy(f))
    #then audio- and midi-buses.
    for f in root.devices["audio-engine"]["audio-bus"]:
        if baseroot.devices["audio-engine"].find("audio-bus[@name='%s']"%f.attrib["name"]) == None:
            print "new audio-engine"
            baseroot.devices["audio-engine"].append(deepcopy(f)) 

    for f in root.devices["midi-engine"]["midi-bus"]:
        if baseroot.devices["midi-engine"].find("midi-bus[@name='%s']"%f.attrib["name"]) == None:
            print "new midi-engine"
            baseroot.devices["midi-engine"].append(deepcopy(f)) 

    #markers
    if root.markers.countchildren():
        for m in root.markers.marker:
            frame = int(m.attrib["frame"])
            frame += project_end_now
            m.attrib["frame"]=str(frame)
            baseroot.markers.append(deepcopy(m))
    
    #project tempo first
    baseroot["tempo-map"].append(temponode_from_project(root))

    #then tempo map
    if root["tempo-map"].countchildren():
        for t in iter(root["tempo-map"]["tempo-node"]):
            frame = int(t.attrib["frame"])
            frame += project_end_now
            t.attrib["frame"]=str(frame)
            baseroot["tempo-map"].append(deepcopy(t))
    #clips
    for t in iter(root.tracks.track):
        thistrack = baseroot.tracks.find("track[@name='%s']"%t.attrib["name"])
        #thistrack = baseroot.tracks.track[t.getparent().index(t)-1]
        #.track[t.getparent().index(t)-1]
        if thistrack == None: #must create new track, but it can be copied directly
            thistrack = deepcopy(t) 
            baseroot.tracks.append(thistrack)
            if thistrack.clips.countchildren():
                for c in iter(thistrack.clips.clip):
                    c.properties.start += project_end_now
        else:
            if t.clips.countchildren():
                for c in iter(t.clips.clip):
                    #if c.attrib["name"] == "Lead_1-6":
                    #    print c.properties.start,c.properties.offset,c.properties.length,project_end_now
                    c.properties.start += project_end_now
                    thistrack.clips.append(deepcopy(c))
    project_end_now += subprojectlength


def are_clips_similar(c1,c2):
    diff = 0
    c1midi = c2midi = False
    if c1.find("midi-clip") != None:
        c1midi = True
    if c2.find("midi-clip") != None:
        c2midi = True
    diff += c1midi != c2midi
    c1p = c1.properties
    c2p = c2.properties
    diff += c1p.gain != c2p.gain
    diff += c1p.name != c2p.name
    if c1midi:
        diff += c1["midi-clip"].filename != c2["midi-clip"].filename
        diff += c1["midi-clip"]["track-channel"] != c2["midi-clip"]["track-channel"]
        diff += c1["midi-clip"]["revision"] != c2["midi-clip"]["revision"]
    else:
        diff += c1["audio-clip"].filename != c2["audio-clip"].filename
        diff += c1["audio-clip"]["time-stretch"] != c2["audio-clip"]["time-stretch"]
        diff += c1["audio-clip"]["pitch-shift"]!= c2["audio-clip"]["pitch-shift"]
    return diff == 0

def join(c1,c2):
    c1p = c1.properties
    c1start = c1p.start
    c1length = c1p.length
    c1end = c1start+c1length
    c1offset = c1p.offset
    if c2 != None:
        c2p = c2.properties
        if are_clips_similar(c1,c2):
            c2start = c2p.start
            c2length = c2p.length
            c2end = c2start+c2length
            c2offset = c2p.offset
            if c1end == c2start and c2offset == c1offset+c1length:
                c1p.length += c2length
                c2.getparent().remove(c2)
                return True
    return False


#MAIN CODE.

for qtr in qtrs[1:]:
    import re
    if re.match(".*\.[0-9]*\.qtr$",qtr) == None: #no backups, please
        print "reading", qtr
        process_qtr(qtr)

#if possible, join clips
for t in iter(baseroot.tracks.track):
    if t.clips.countchildren():
        continueloop = True
        while continueloop:
            continueloop = False
            for c1 in iter(t.clips.clip):
                c2 = c1.getnext()
                if join(c1,c2):
                    continueloop = True
                    break #must start again, because clips have been modified

ofname= sys.argv[1]
print "writing", ofname
of = open(ofname,"w")
of.write(etree.tostring(baseroot,pretty_print=True))
of.close()
