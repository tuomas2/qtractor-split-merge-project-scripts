(C) 2013 Tuomas Airaksinen
Licence: GPL, see file COPYING

Related project: http://qtractor.sourceforge.net/

split_qtractor_project.py : 
	split project with respect to markers 
	(splits clips as well at markers).
merge_qtractor_projects.py: 
	join multiple projects into one. 
	This also joins clips, if they can be joined.

Requires python-lxml
