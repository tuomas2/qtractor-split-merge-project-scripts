#!/usr/bin/python 
# encoding: utf-8
# (C) 2013, Tuomas Airaksinen
# Licenced under GPL
# 
# Split one QTractor projects into many with respect to markers.
# 
# Markers containing string "[NS]" (no-split) are ignored.
#
# Requires python-lxml package.
# 

from lxml import etree,objectify
from copy import deepcopy
import sys

if len(sys.argv) != 2: 
    print sys.argv[0],"fname"
    sys.exit(0)

fname = sys.argv[1]

root = objectify.parse(fname).getroot()

def split(clip,frame):
    start = clip.properties.start
    length = clip.properties.length
    end  = start + length
    
    #if clip is before or after frame
    if end <= frame or start >= frame:
        return None

    c1 = deepcopy(clip)
    c2 = deepcopy(clip)
    
    c1.properties.length -= end-frame
    
    c2.properties.start += frame-start
    c2.properties.offset += frame-start
    c2.properties.length -= frame-start
    assert c1.properties.length != 0
    assert c2.properties.length != 0
    
    parent = clip.getparent()
    clip.addnext(c1)
    clip.addnext(c2)
    parent.remove(clip)
    return True

def handle_marker(item,startframe,endframe):
    parent = item.getparent()
    itemframe = int(item.attrib["frame"])
    if itemframe >= startframe and itemframe < endframe:
        itemframe -= startframe
        item.attrib["frame"] = str(itemframe)
    else:
        parent.remove(item)

def handle_clip(item,path,name,startframe,endframe):
    parent = item.getparent()
    itemframe = item.find(path)[name]
    if itemframe >= startframe and itemframe < endframe:
        item.find(path)[name] -= startframe
    else:
        parent.remove(item)

def writemodxml(modroot,startframe,endframe,fname,counter,name):
    lasttempo = None
    for t in iter(modroot.tracks.track):
        if t.clips.countchildren():
            for c in iter(t.clips.clip):
                handle_clip(c,"properties","start",startframe,endframe)

    for m1 in iter(modroot.markers.marker): 
        handle_marker(m1,startframe,endframe)

    for t in iter(modroot["tempo-map"]["tempo-node"]):
        t.attrib["bar"] = "-1"
        tempoframe = int(t.attrib["frame"])
        if tempoframe < startframe:
            lasttempo = deepcopy(t)
        handle_marker(t,startframe,endframe)
    
    try:
        temponode = modroot["tempo-map"]["tempo-node"][0]
    except AttributeError:
        temponode = -1

    if temponode != -1 and int(temponode.attrib["frame"]) == 0:
        lasttempo = deepcopy(temponode)
        modroot["tempo-map"].remove(temponode)

    if lasttempo != None:
        modroot.properties.tempo = lasttempo.tempo
        modroot.properties["beats-per-bar"] = lasttempo["beats-per-bar"]
        modroot.properties["beat-divisor"] = lasttempo["beat-divisor"]

    ofname= fname[:-4] + "_split_%.2d_%s.%s"%(counter,name,fname[-3:]) 
    print "writing", ofname
    of = open(ofname,"w")
    of.write(etree.tostring(modroot,pretty_print=True))
    of.close()

def process_qtr(fname):
    startframe = 0
    projectlength = 0
    counter = 0
    name = "first"

    for c in root.findall("tracks/track/clips/clip"):
        end = c.properties.start+c.properties.length
        if end > projectlength:
            projectlength = end

    for m in root.markers.marker:
        for c in root.findall("tracks/track/clips/clip"):
            frame = int(m.attrib["frame"])
            split(c,frame)

    for m in root.markers.marker:
        if "[NS]" in unicode(m["text"]):
            print "skipping", unicode(m["text"])
            continue #no split
        frame = int(m.attrib["frame"])
        modroot = deepcopy(root)
        endframe = frame
        writemodxml(modroot,startframe,endframe,fname,counter,name)
        startframe = frame
        counter+=1
        name = unicode(m["text"]).replace(" ","_")
    
    modroot = deepcopy(root)
    writemodxml(modroot,startframe,projectlength,fname,counter,name)

process_qtr(fname)
